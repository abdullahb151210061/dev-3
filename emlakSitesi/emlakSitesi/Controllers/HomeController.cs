﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using emlakSitesi.Models;

namespace emlakSitesi.Controllers
{
    public class HomeController : Controller
    {
        private emlaklarDbContext db = new emlaklarDbContext();
        // GET: Home
        public ActionResult Index()
        {
            
            return View(db.emlaklar.ToList());
        }

        public ActionResult Emlaklar()
        {
            return View();
        }

        public ActionResult Satilik()
        {
            return View(db.emlaklar.ToList());
        }

        public ActionResult Kiralik()
        {
            return View(db.emlaklar.ToList());
        }
    }
}