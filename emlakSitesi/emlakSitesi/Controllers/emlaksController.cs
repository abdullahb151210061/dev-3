﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using emlakSitesi.Models;

namespace emlakSitesi.Controllers
{
    public class emlaksController : Controller
    {
        private emlaklarDbContext db = new emlaklarDbContext();

        // GET: emlaks
        public ActionResult Index(string searchString)
        {
            var emlaklar = from m in db.emlaklar
                         select m;

            if (!String.IsNullOrEmpty(searchString))
            {
                emlaklar = emlaklar.Where(s => s.emlakBaslik.Contains(searchString));
            }

            return View(emlaklar);
        }

        // GET: emlaks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            emlak emlak = db.emlaklar.Find(id);
            if (emlak == null)
            {
                return HttpNotFound();
            }
            return View(emlak);
        }

        // GET: emlaks/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: emlaks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "emlakID,emlakBaslik,emlakAciklama,emlakTuru,emlakOnay,emlakFiyat,emlakResim,il,ilce")] emlak emlak)
        {
            if (ModelState.IsValid)
            {
                db.emlaklar.Add(emlak);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(emlak);
        }

        // GET: emlaks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            emlak emlak = db.emlaklar.Find(id);
            if (emlak == null)
            {
                return HttpNotFound();
            }
            return View(emlak);
        }

        // POST: emlaks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "emlakID,emlakBaslik,emlakAciklama,emlakTuru,emlakOnay,emlakFiyat,emlakResim,il,ilce")] emlak emlak)
        {
            if (ModelState.IsValid)
            {
                db.Entry(emlak).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(emlak);
        }

        // GET: emlaks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            emlak emlak = db.emlaklar.Find(id);
            if (emlak == null)
            {
                return HttpNotFound();
            }
            return View(emlak);
        }

        // POST: emlaks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            emlak emlak = db.emlaklar.Find(id);
            db.emlaklar.Remove(emlak);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
