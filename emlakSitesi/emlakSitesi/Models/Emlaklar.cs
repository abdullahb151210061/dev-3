﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace emlakSitesi.Models
{
    public class emlak
    {

        public int emlakID { get; set; }

        public string emlakBaslik { get; set; }

        public string emlakAciklama { get; set; }

        public int emlakTuru { get; set; }

        public int emlakOnay { get; set; }

        public int emlakFiyat { get; set; }

        public string emlakResim { get; set; }

        public string il { get; set; }

        public string ilce { get; set; }
    }

    public class emlaklarDbContext: DbContext
    {
        public DbSet<emlak> emlaklar { get; set; }
    }
}